﻿namespace Referrals.Database.Entities
{
    public class Referral : BaseEntity<long>
    {
        public long ReferrerId { get; set; }
        public long ReferreeId { get; set; }
        public long ReferralPlanId { get; set; }

        public virtual User Referrer { get; set; }
        public virtual User Referree { get; set; }
        public virtual ReferralPlan ReferralPlan { get; set; }
    }
}
