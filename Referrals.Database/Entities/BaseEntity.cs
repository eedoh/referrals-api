﻿using System;

namespace Referrals.Database.Entities
{
    public abstract class BaseEntity<T> : IBaseEntity<T>
    {
        public T Id { get; set; }
        object IBaseEntity.Id
        {
            get { return Id; }
        }

        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public long? CreatedById { get; set; }
        public long? ModifiedById { get; set; }
    }
}
