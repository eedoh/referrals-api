﻿namespace Referrals.Database.Entities
{
    public class ReferralPlan : BaseEntity<long>
    {
        public long UserId { get; set; }
        public decimal Amount { get; set; }
        public int PayoutUnitSize { get; set; }
        public string ReferralCode { get; set; }
        public decimal SignupBonus { get; set; }

        public User User { get; set; }
    }
}
