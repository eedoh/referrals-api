﻿using System.Collections.Generic;

namespace Referrals.Database.Entities
{
    public class User : BaseEntity<long>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CognitoId { get; set; }

        public ICollection<Referral> Referrals { get; set; }
        public Referral ReferredBy { get; set; }
    }
}
