﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Referrals.Database.Configs
{
    public class BaseEntityConfiguration<T, U> : IEntityTypeConfiguration<T> where T : class, IBaseEntity<U>
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id); // set the id column as primary key for each table
            builder.Property(x => x.Id).HasColumnName("id").ValueGeneratedOnAdd().UseIdentityColumn(); // make sure id value is being automatically generated if not provided

            builder.Property(x => x.CreatedById).HasColumnName("created_by_id");
            builder.Property(x => x.ModifiedById).HasColumnName("modified_by_id");
            builder.Property(x => x.CreatedDate).HasColumnName("created_date").HasDefaultValueSql("now() at time zone 'utc'");
            builder.Property(x => x.ModifiedDate).HasColumnName("modified_date").HasDefaultValueSql("now() at time zone 'utc'");
        }
    }
}
