﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Referrals.Database.Entities;

namespace Referrals.Database.Configs
{
    public class UserEntityConfiguration : BaseEntityConfiguration<User, long>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);
            builder.ToTable("users");

            builder.Property(x => x.FirstName).HasColumnName("first_name").IsRequired().HasMaxLength(50);
            builder.Property(x => x.LastName).HasColumnName("last_name").IsRequired().HasMaxLength(50);
            builder.Property(x => x.Email).HasColumnName("email").IsRequired();
            builder.Property(x => x.Phone).HasColumnName("phone");

            builder.HasIndex(u => u.Email).IsUnique();
        }
    }
}
