﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Referrals.Database.Configs;
using Referrals.Database.Entities;

namespace Referrals.Database.EntityConfigs
{
    public class ReferralPlanEntityConfiguration : BaseEntityConfiguration<ReferralPlan, long>
    {
        public override void Configure(EntityTypeBuilder<ReferralPlan> builder)
        {
            base.Configure(builder);
            builder.ToTable("referral_plans");

            builder.Property(x => x.UserId).HasColumnName("user_id").IsRequired();
            builder.Property(x => x.Amount).HasColumnName("amount").IsRequired();
            builder.Property(x => x.PayoutUnitSize).HasColumnName("payout_unit_size").IsRequired();
            builder.Property(x => x.ReferralCode).HasColumnName("referral_code").IsRequired();

            builder.HasIndex(x => x.ReferralCode).IsUnique();
        }
    }
}
