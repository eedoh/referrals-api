﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Referrals.Database.Configs;
using Referrals.Database.Entities;

namespace Referrals.Database.EntityConfigs
{
    public class ReferralEntityConfiguration : BaseEntityConfiguration<Referral, long>
    {
        public override void Configure(EntityTypeBuilder<Referral> builder)
        {
            base.Configure(builder);
            builder.ToTable("referrals");

            builder.Property(x => x.ReferrerId).HasColumnName("referrer_id").IsRequired();
            builder.Property(x => x.ReferreeId).HasColumnName("referree_id").IsRequired();
            builder.Property(x => x.ReferralPlanId).HasColumnName("referral_plan_id").IsRequired();

            // Make sure new user can be referred only by a single other user (unique key constraint)
            builder.HasIndex(r => r.ReferreeId).IsUnique(); 

            // EF Core can't figure out these relationships based on their names and their types (User => Referrer/Referree) as it usually does, so they need to be explicity specified
            builder.HasOne(r => r.ReferralPlan).WithMany().HasForeignKey(r => r.ReferralPlanId);
            builder.HasOne(r => r.Referrer).WithMany(u => u.Referrals).HasForeignKey(r => r.ReferrerId);
            builder.HasOne(r => r.Referree).WithOne(u => u.ReferredBy).HasForeignKey<Referral>(u => u.ReferreeId);
        }
    }
}
