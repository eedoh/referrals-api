﻿using System;

namespace Referrals.Database
{
    public interface IBaseEntity
    {
        object Id { get; }
        DateTime CreatedDate { get; set; }
        DateTime ModifiedDate { get; set; }
        long? CreatedById { get; set; }
        long? ModifiedById { get; set; }
    }

    public interface IBaseEntity<T> : IBaseEntity
    {
        new T Id { get; set; }
    }
}
