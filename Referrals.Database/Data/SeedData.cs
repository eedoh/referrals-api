﻿using Microsoft.EntityFrameworkCore;
using Referrals.Database.Entities;

namespace Referrals.Database
{
    public static class SeedData
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1L,
                    Email = "admin@yopmail.com",
                    FirstName = "Admin",
                    LastName = "Admin",
                    CognitoId = "372bb514-bdb2-400c-9731-3a9fd7491667"
                },
                new User
                {
                    Id = 2L,
                    Email = "alice@wonderland.com",
                    FirstName = "Alice",
                    LastName = "Von Wonderland",
                    CognitoId = "9904e3f9-04e0-4b74-aa58-82ab1236173a"
                }
            );
            modelBuilder.Entity<ReferralPlan>().HasData(
               new ReferralPlan
               {
                   Id = 1L,
                   Amount = 10,
                   PayoutUnitSize = 5,
                   ReferralCode = "$10FOR5USERS",
                   SignupBonus = 10,
                   UserId = 2L
               }
           );
        }
    }
}
