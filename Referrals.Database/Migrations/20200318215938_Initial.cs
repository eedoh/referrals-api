﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Referrals.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    modified_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    created_by_id = table.Column<long>(nullable: true),
                    modified_by_id = table.Column<long>(nullable: true),
                    first_name = table.Column<string>(maxLength: 50, nullable: false),
                    last_name = table.Column<string>(maxLength: 50, nullable: false),
                    email = table.Column<string>(nullable: false),
                    phone = table.Column<string>(nullable: true),
                    CognitoId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "referral_plans",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    modified_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    created_by_id = table.Column<long>(nullable: true),
                    modified_by_id = table.Column<long>(nullable: true),
                    user_id = table.Column<long>(nullable: false),
                    amount = table.Column<decimal>(nullable: false),
                    payout_unit_size = table.Column<int>(nullable: false),
                    referral_code = table.Column<string>(nullable: false),
                    SignupBonus = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_referral_plans", x => x.id);
                    table.ForeignKey(
                        name: "FK_referral_plans_users_user_id",
                        column: x => x.user_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "referrals",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    created_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    modified_date = table.Column<DateTime>(nullable: false, defaultValueSql: "now() at time zone 'utc'"),
                    created_by_id = table.Column<long>(nullable: true),
                    modified_by_id = table.Column<long>(nullable: true),
                    referrer_id = table.Column<long>(nullable: false),
                    referree_id = table.Column<long>(nullable: false),
                    referral_plan_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_referrals", x => x.id);
                    table.ForeignKey(
                        name: "FK_referrals_referral_plans_referral_plan_id",
                        column: x => x.referral_plan_id,
                        principalTable: "referral_plans",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_referrals_users_referree_id",
                        column: x => x.referree_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_referrals_users_referrer_id",
                        column: x => x.referrer_id,
                        principalTable: "users",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "id", "CognitoId", "created_by_id", "email", "first_name", "last_name", "modified_by_id", "phone" },
                values: new object[,]
                {
                    { 1L, "372bb514-bdb2-400c-9731-3a9fd7491667", null, "admin@yopmail.com", "Admin", "Admin", null, null },
                    { 2L, "9904e3f9-04e0-4b74-aa58-82ab1236173a", null, "alice@wonderland.com", "Alice", "Von Wonderland", null, null }
                });

            migrationBuilder.InsertData(
                table: "referral_plans",
                columns: new[] { "id", "amount", "created_by_id", "modified_by_id", "payout_unit_size", "referral_code", "SignupBonus", "user_id" },
                values: new object[] { 1L, 10m, null, null, 5, "$10FOR5USERS", 10m, 2L });

            migrationBuilder.CreateIndex(
                name: "IX_referral_plans_referral_code",
                table: "referral_plans",
                column: "referral_code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_referral_plans_user_id",
                table: "referral_plans",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_referrals_referral_plan_id",
                table: "referrals",
                column: "referral_plan_id");

            migrationBuilder.CreateIndex(
                name: "IX_referrals_referree_id",
                table: "referrals",
                column: "referree_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_referrals_referrer_id",
                table: "referrals",
                column: "referrer_id");

            migrationBuilder.CreateIndex(
                name: "IX_users_email",
                table: "users",
                column: "email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "referrals");

            migrationBuilder.DropTable(
                name: "referral_plans");

            migrationBuilder.DropTable(
                name: "users");
        }
    }
}
