﻿using Microsoft.EntityFrameworkCore;
using Referrals.Database.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Referrals.Database
{
    public interface IReferralsDBContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<ReferralPlan> ReferralPlans { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
