﻿using Microsoft.EntityFrameworkCore;
using Referrals.Database.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Referrals.Database
{
    public class ReferralsDBContext : DbContext, IReferralsDBContext
    {
        public ReferralsDBContext(DbContextOptions<ReferralsDBContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Referral> Referrals { get; set; }
        public DbSet<ReferralPlan> ReferralPlans { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(typeof(ReferralsDBContext).Assembly);
            SeedData.Seed(builder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
