﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Services;
using Referrals.Services.AuthContext;
using Referrals.Tests.Mocks;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Tests
{
    public class ReferralsServiceTests
    {
        private Mock<ILogger<ReferralsService>> _loggerMock;
        private Mock<IAuthContext> _authContextMock;

        DbContextOptions<ReferralsDBContext> _options;
        ReferralsDBContext _context;

        [SetUp]
        public void Setup()
        {
            _loggerMock = new Mock<ILogger<ReferralsService>>();

            _options = new DbContextOptionsBuilder<ReferralsDBContext>()
                 .UseInMemoryDatabase(databaseName: "referrals")
                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                 .Options;

            _context = TestData.Get(_options);

            _authContextMock = new Mock<IAuthContext>();
            _authContextMock.Setup(x => x.GetUser())
                    .ReturnsAsync(new AuthContextUserDTO()
                    {
                        Id = 1,
                        CognitoId = "",
                        Group = "Admin"
                    });
        }

        [Test]
        public async Task GetCredit_UserDoesNotExist()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.ThrowsAsync<ArgumentException>(
                async () => await referralsService.GetCredit(1000));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetCredit_ReferralsOnly()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetCredit(1));

            var result = await referralsService.GetCredit(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == 14);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetCredit_SignupBonusOnly()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetCredit(2));

            var result = await referralsService.GetCredit(2);
            Assert.IsNotNull(result);
            Assert.IsTrue(result == 10);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public void GetReferrals_UserDoesNotExist()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.ThrowsAsync<ArgumentException>(
                async () => await referralsService.GetReferrals(1000));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetReferrals_HasReferrals()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetReferrals(1));

            var result = await referralsService.GetReferrals(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ToList().Count > 0);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetReferrals_HasNoReferrals()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetReferrals(2));

            var result = await referralsService.GetReferrals(2);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ToList().Count == 0);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public void GetEarningDetails_UserDoesNotExist()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.ThrowsAsync<ArgumentException>(
                async () => await referralsService.GetEarningDetails(1000));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetEarningDetails_HasReferrals()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetEarningDetails(1));

            var result = await referralsService.GetEarningDetails(1);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Referrals.ToList().Count > 0);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetEarningDetails_HasSignupBonus()
        {
            var referralsService = new ReferralsService(
                      _context,
                      _loggerMock.Object,
                      _authContextMock.Object);


            Assert.DoesNotThrowAsync(
                async () => await referralsService.GetEarningDetails(2));

            var result = await referralsService.GetEarningDetails(2);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Signup.SignupBonus > 0);

            _context.Database.EnsureDeleted();
        }
    }
}
