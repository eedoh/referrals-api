﻿using Amazon.CognitoIdentityProvider.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Services;
using Referrals.Services.AuthContext;
using Referrals.Services.Interfaces;
using Referrals.Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Referrals.Tests
{
    public class AuthServiceTests
    {
        private Mock<ILogger<AuthService>> _loggerMock;
        private Mock<ILogger<ReferralsService>> _referralsLoggerMock;
        private Mock<IAWSCognitoService> _awsCognitoServiceMock;
        private Mock<IAuthContext> _authContextMock;

        private IReferralsService _referralsService;
        private IUsersService _usersService;
        private IAuthService _authService;

        DbContextOptions<ReferralsDBContext> _options;
        ReferralsDBContext _context;

        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<ReferralsDBContext>()
                 .UseInMemoryDatabase(databaseName: "referrals")
                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                 .Options;

            _context = TestData.Get(_options);

            _authContextMock = new Mock<IAuthContext>();
            _authContextMock.Setup(x => x.GetUser())
                    .ReturnsAsync(new AuthContextUserDTO()
                    {
                        Id = 1,
                        CognitoId = "",
                        Group = "Admin"
                    });

            _referralsLoggerMock = new Mock<ILogger<ReferralsService>>();
            _referralsService = new ReferralsService(_context, _referralsLoggerMock.Object, _authContextMock.Object);
            _usersService = new UsersService(_context, _authContextMock.Object, _referralsService);

            _loggerMock = new Mock<ILogger<AuthService>>();

            var userTypeAttributes = new List<AttributeType>();
            userTypeAttributes.Add(new AttributeType
            {
                Name = "sub",
                Value = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
            });
            _awsCognitoServiceMock = new Mock<IAWSCognitoService>();
            _awsCognitoServiceMock.Setup(x => x.SignUpUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<AttributeType>>()))
                .ReturnsAsync(new AdminCreateUserResponse
                {
                    User = new UserType
                    {
                        Attributes = userTypeAttributes
                    }
                });

            _authService = new AuthService(
                _context,
                _loggerMock.Object,
                _awsCognitoServiceMock.Object,
                _referralsService,
                _usersService);
        }

        [TearDown]
        public void Teardown()
        {
            _context.Database.EnsureDeleted();
        }

        [Test]
        public void Signup_MissingName()
        {
            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test123!",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));

            Assert.IsNotNull(exception);
        }

        [Test]
        public void Signup_MissingEmailAddress()
        {
            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New Alice",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));

            Assert.IsNotNull(exception);
        }

        [Test]
        public void Signup_IncorrectEmailAddressFormat()
        {
            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny_at_new.user",
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New Alice",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));

            Assert.IsNotNull(exception);
        }

        [Test]
        public void Signup_EmailAddressAlreadyInUse()
        {
            var exception = Assert.ThrowsAsync<Exception>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "alice@wonderland.com",
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New Alice",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));

            Assert.IsNotNull(exception);
        }

        [Test]
        public void Signup_NoReferralCode()
        {
            var referralsCount = _context.Referrals.Count();

            Assert.DoesNotThrowAsync(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123"
            }));
            Assert.AreEqual(referralsCount, _context.Referrals.Count());
            Assert.IsTrue(_context.Users.Any(u => u.Email == "shiny@new.user"));
        }

        [Test]
        public void Signup_InvalidReferralCode()
        {
            var userReferralsCount = _context.Referrals.Count(r => r.ReferrerId == 1);

            var exception = Assert.ThrowsAsync<Exception>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123",
                ReferralCode = "$10FOR10USERS"
            }));

            Assert.IsNotNull(exception);
            Assert.AreEqual(userReferralsCount, _context.Referrals.Count(r => r.ReferrerId == 1));
        }

        [Test]
        public void Signup_ValidReferralCode()
        {
            var userReferralsCount = _context.Referrals.Count(r => r.ReferrerId == 1);

            Assert.DoesNotThrowAsync(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test123!",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));
            Assert.Greater(_context.Referrals.Count(r => r.ReferrerId == 1), userReferralsCount);
        }

        [Test]
        public void Signup_PasswordTooShort()
        {
            var userReferralsCount = _context.Referrals.Count(r => r.ReferrerId == 1);

            Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test12!",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));
            Assert.AreEqual(_context.Referrals.Count(r => r.ReferrerId == 1), userReferralsCount);
        }

        [Test]
        public void Signup_PasswordMissingSpecialCharacter()
        {
            var userReferralsCount = _context.Referrals.Count(r => r.ReferrerId == 1);

            Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "Test123",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));
            Assert.AreEqual(_context.Referrals.Count(r => r.ReferrerId == 1), userReferralsCount);
        }

        [Test]
        public void Signup_PasswordMissingANumber()
        {
            var userReferralsCount = _context.Referrals.Count(r => r.ReferrerId == 1);

            Assert.ThrowsAsync<FluentValidation.ValidationException>(async () => await _authService.Signup(new Referrals_API.DTOs.SignupRequestDTO()
            {
                Email = "shiny@new.user",
                Password = "TestTest!",
                FirstName = "Shiny",
                LastName = "New User",
                Phone = "123123123",
                ReferralCode = "$10FOR5USERS"
            }));
            Assert.AreEqual(_context.Referrals.Count(r => r.ReferrerId == 1), userReferralsCount);
        }
    }
}
