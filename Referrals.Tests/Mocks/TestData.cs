﻿using Microsoft.EntityFrameworkCore;
using Referrals.Database;
using Referrals.Database.Entities;
using System;
using System.Collections.Generic;

namespace Referrals.Tests.Mocks
{
    public static class TestData
    {
        public static ReferralsDBContext Get(DbContextOptions<ReferralsDBContext> options)
        {
            var referralPlans = new List<ReferralPlan>();
            referralPlans.Add(new ReferralPlan()
            {
                Id = 1,
                UserId = 1,
                PayoutUnitSize = 5,
                Amount = 10,
                SignupBonus = 10,
                ReferralCode = "$10FOR5USERS"
            });
            referralPlans.Add(new ReferralPlan()
            {
                Id = 2,
                UserId = 1,
                PayoutUnitSize = 2,
                Amount = 7,
                SignupBonus = 10,
                ReferralCode = "$7FOR2USERS"
            });

            var referrals = new List<Referral>();
            for (int i = 1; i < 5; i++)
            {
                var referral = new Referral()
                {
                    Id = i,
                    ReferralPlanId = 1,
                    ReferrerId = 1,
                    ReferreeId = i + 1,
                    ReferralPlan = referralPlans[0],
                    Referree = new User()
                    {
                        Id = i + 1,
                        FirstName = string.Format("FirstName_{0}", i + 1),
                        LastName = string.Format("LastName_{0}", i + 1),
                        CreatedDate = DateTime.Now.AddDays(i * (-1))
                    }
                };
                referrals.Add(referral);
            }
            for (int i = 5; i < 9; i++)
            {
                var referral = new Referral()
                {
                    Id = i,
                    ReferralPlanId = 2,
                    ReferrerId = 1,
                    ReferreeId = i + 1,
                    ReferralPlan = referralPlans[1],
                    Referree = new User()
                    {
                        Id = i + 1,
                        FirstName = string.Format("FirstName_{0}", i + 1),
                        LastName = string.Format("LastName_{0}", i + 1),
                        CreatedDate = DateTime.Now.AddDays(i * (-1))
                    }
                };
                referrals.Add(referral);
            }

            var user = new User()
            {
                Id = 1,
                FirstName = "Alice",
                LastName = "Von Wonderland",
                Email = "alice@wonderland.com",
                Phone = "123123123",
                ReferredBy = null,
                Referrals = referrals
            };

            var context = new ReferralsDBContext(options);
            context.Referrals.AddRange(referrals);
            context.ReferralPlans.AddRange(referralPlans);
            context.Users.Add(user);
            context.SaveChanges();

            return context;
        }
    }
}
