﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Services;
using Referrals.Services.AuthContext;
using Referrals.Tests.Mocks;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Tests
{
    public class ReferralPlansServiceTests
    {
        private Mock<ILogger<HelperService>> _helperServiceLoggerMock;
        private Mock<ILogger<ReferralPlansService>> _loggerMock;
        
        private IHelperService _helperService;
        private Mock<IAuthContext> _authContextMock;
        
        private ReferralPlansService _referralPlansService;

        DbContextOptions<ReferralsDBContext> _options;
        ReferralsDBContext _context;

        [SetUp]
        public void Setup()
        {
            _options = new DbContextOptionsBuilder<ReferralsDBContext>()
                 .UseInMemoryDatabase(databaseName: "referrals")
                 .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                 .Options;
            _context = TestData.Get(_options);

            _loggerMock = new Mock<ILogger<ReferralPlansService>>();

            _helperServiceLoggerMock = new Mock<ILogger<HelperService>>();
            _helperService = new HelperService(_context, _helperServiceLoggerMock.Object);

            _authContextMock = new Mock<IAuthContext>();
            _authContextMock.Setup(x => x.GetUser())
                    .ReturnsAsync(new AuthContextUserDTO()
                    {
                        Id = 1,
                        CognitoId = "",
                        Group = "Admin"
                    });

            _referralPlansService = new ReferralPlansService(
                      _context,
                      _helperService,
                      _authContextMock.Object,
                      _loggerMock.Object);
        }

        [Test]
        public async Task GetAllReferralPlans()
        {
            Assert.DoesNotThrowAsync(
                    async () => await _referralPlansService.GetReferralPlans());

            var referralPlans = await _referralPlansService.GetReferralPlans();
            Assert.IsNotNull(referralPlans);
            Assert.IsTrue(referralPlans.Count() > 0);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public void GetReferralPlans_UserDoesNotExist()
        {
            var exception = Assert.ThrowsAsync<ArgumentException>(
                    async () => await _referralPlansService.GetReferralPlans(1000));
            Assert.IsNotNull(exception);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task GetReferralPlans()
        {
            Assert.DoesNotThrowAsync(
                    async () => await _referralPlansService.GetReferralPlans(1));

            var referralPlans = await _referralPlansService.GetReferralPlans(1);
            Assert.IsNotNull(referralPlans);
            Assert.IsTrue(referralPlans.Count() > 0);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_BrandPhraseTooLong()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 10,
                BrandPhrase = "AMITREE_FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = 5,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_MissingRequiredInformation()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                UserId = 1,
                BrandPhrase = "FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = 5
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_NegativeAmount()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = -10,
                BrandPhrase = "AMITREE_FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = 5,
                SignupBonus = 10,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_NegativeCodeLength()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 10,
                BrandPhrase = "FOLIO_",
                CodeLength = -10,
                PayoutUnitSize = 5,
                SignupBonus = 10,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_NegativePayoutUnitSize()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 10,
                BrandPhrase = "FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = -5,
                SignupBonus = 10,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_NegativeSignupBonus()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 10,
                BrandPhrase = "AMITREE_FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = 5,
                SignupBonus = -10,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<FluentValidation.ValidationException>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);
            Assert.IsFalse(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }

        [Test]
        public void CreateNewReferralPlan_ReferralCodeAlreadyExists()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 10,
                BrandPhrase = "$10FOR5USERS",
                CodeLength = 12,
                PayoutUnitSize = 5,
                SignupBonus = 10,
                UserId = 1
            };

            var exception = Assert.ThrowsAsync<Exception>(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsNotNull(exception);

            _context.Database.EnsureDeleted();
        }

        [Test]
        public async Task CreateNewReferralPlan_Valid()
        {
            var newReferralPlanRequest = new NewReferralPlanRequestDTO()
            {
                Amount = 30,
                BrandPhrase = "FOLIO_",
                CodeLength = 10,
                PayoutUnitSize = 10,
                SignupBonus = 10,
                UserId = 1
            };

            Assert.DoesNotThrowAsync(
                async () => await _referralPlansService.CreateNewReferralPlan(newReferralPlanRequest));

            Assert.IsTrue(await _context.ReferralPlans.AnyAsync(rc =>
                rc.UserId == newReferralPlanRequest.UserId
                && rc.Amount == newReferralPlanRequest.Amount
                && rc.PayoutUnitSize == newReferralPlanRequest.PayoutUnitSize
                && rc.ReferralCode.Contains(newReferralPlanRequest.BrandPhrase)));

            _context.Database.EnsureDeleted();
        }
    }
}
