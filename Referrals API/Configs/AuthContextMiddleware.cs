﻿using Microsoft.AspNetCore.Http;
using Referrals.Services.AuthContext;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals_API.Configs
{
    public class AuthContextMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        // Intercept every request
        public async Task Invoke(HttpContext httpContext, IAuthContext authContext)
        {
            authContext.Data = new Dictionary<string, object>();

            // Get all the headers and forward it to the AuthContext
            foreach (var item in httpContext.Request.Headers)
                authContext.Data.Add(item.Key, item.Value);

            // Proceed with the request
            await _next(httpContext);
        }
    }
}
