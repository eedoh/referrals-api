﻿using Microsoft.AspNetCore.Http;
using Serilog;
using Serilog.Events;
using System;
using System.Diagnostics;
using System.Linq;

namespace Referrals_API.Configs
{
    public class SerilogRequestLogging
    {
        public static string HostingEnvironmentVariable { get; } = "ASPNETCORE_ENVIRONMENT";
        public static void SerilogSetup(string[] args)
        {
            var loggerConfig = new LoggerConfiguration();
            Serilog.Debugging.SelfLog.Enable(msg => Debug.WriteLine(msg));
            loggerConfig = loggerConfig.MinimumLevel.Debug().WriteTo.Console(LogEventLevel.Debug);
            Log.Logger = loggerConfig.Enrich.FromLogContext().CreateLogger();
        }

        public static void EnrichFromRequest(IDiagnosticContext diagnosticContext, HttpContext httpContext)
        {
            var request = httpContext.Request;

            // Set all the common properties available for every request
            diagnosticContext.Set("Host", request.Host);
            diagnosticContext.Set("Protocol", request.Protocol);
            diagnosticContext.Set("Scheme", request.Scheme);

            // Only set it if available.
            if (request.QueryString.HasValue)
            {
                diagnosticContext.Set("QueryString", request.QueryString.Value);
            }

            // Set the content-type of the Response at this point
            diagnosticContext.Set("ContentType", httpContext.Response.ContentType);

            // Retrieve the IEndpointFeature selected for the request
            var endpoint = httpContext.GetEndpoint();
            if (endpoint is object)
            {
                diagnosticContext.Set("EndpointName", endpoint.DisplayName);
            }
        }
        
        public static Func<HttpContext, double, Exception, LogEventLevel> GetLevel(LogEventLevel traceLevel, params string[] traceEndpointNames)
        {
            if (traceEndpointNames is null || traceEndpointNames.Length == 0)
            {
                throw new ArgumentNullException(nameof(traceEndpointNames));
            }

            return (ctx, _, ex) =>
            {
                if (IsError(ctx, ex))
                {
                    return LogEventLevel.Error;
                }

                if (IsTraceEndpoint(ctx, traceEndpointNames))
                {
                    return traceLevel;
                }

                return LogEventLevel.Information;
            };
        }

        static bool IsError(HttpContext ctx, Exception ex)
            => ex is { } || ctx.Response.StatusCode > 499;

        static bool IsTraceEndpoint(HttpContext ctx, string[] traceEndpoints)
        {
            var endpoint = ctx.GetEndpoint();
            if (endpoint is { })
            {
                return traceEndpoints.Any(x => x.Equals(endpoint.DisplayName, StringComparison.OrdinalIgnoreCase));
            }
            return false;
        }
    }
}
