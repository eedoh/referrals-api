﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace Referrals_API.Configs
{
    public static class AWSCognitoAuthentication
    {
        public static void ConfigureAWSCognitoAuthentication(this IServiceCollection services, IConfiguration configuration)
        {

            var region = configuration["AWSConfig:Region"];
            var poolId = configuration["AWSConfig:UserPoolId"];
            var appClientId = configuration["AWSConfig:AppClientId"];

            services
                .AddAuthentication("Bearer")
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = false,
                        IssuerSigningKeyResolver = (s, securityToken, identifier, parameters) =>
                        {
                            // Get JsonWebKeySet from AWS
                            var json = new WebClient().DownloadString(parameters.ValidIssuer + "/.well-known/jwks.json");
                            // Serialize the result
                            return JsonConvert.DeserializeObject<JsonWebKeySet>(json).Keys;
                        },
                        ValidateIssuer = true,
                        ValidIssuer = $"https://cognito-idp.{region}.amazonaws.com/{poolId}",
                        ValidateLifetime = true,
                        LifetimeValidator = (before, expires, token, param) => expires > DateTime.UtcNow,
                        ValidateAudience = false,
                        ValidAudience = appClientId,
                    };
                });

            services
                .AddAuthorization(options =>
                {
                    options.AddPolicy("Admin", policy => policy.RequireClaim("cognito:groups", new List<string> { "Admin" }));
                    options.AddPolicy("User", policy => policy.RequireClaim("cognito:groups", new List<string> { "User" }));
                });
        }
    }
}
