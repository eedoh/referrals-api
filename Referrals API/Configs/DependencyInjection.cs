﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Referrals.Common.DTOs;
using Referrals.Services;
using Referrals.Services.AuthContext;
using Referrals.Services.Interfaces;

namespace Referrals_API.Configurations
{
    public static class DependencyInjections
    {
        public static void RegisterTypes(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IHelperService, HelperService>();
            services.AddScoped<IReferralPlansService, ReferralPlansService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IReferralsService, ReferralsService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IAWSCognitoService, AWSCognitoService>();
            services.AddScoped<IAuthContext, AuthContext>();
            services.AddScoped<IAuthContextService, AuthContextService>();

            var awsConfig = configuration.GetSection("AWSConfig").Get<AWSConfig>();
            services.AddSingleton(awsConfig);
        }
    }
}
