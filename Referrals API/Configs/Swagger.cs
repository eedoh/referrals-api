﻿using Microsoft.Extensions.DependencyInjection;

namespace Referrals_API.Extensions
{
    public static class Swagger
    {
        public static void ConfigureSwagger(this IServiceCollection services)
        {

            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {

                    document.Info.Version = "0.1";
                    document.Info.Title = "Referrals Api";
                    document.Info.Description = "Amitree take home assignment";
                    document.Info.TermsOfService = "None";
                };
            });
        }
    }
}
