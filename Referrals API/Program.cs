using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Referrals_API.Configs;
using Serilog;
using System;

namespace Referrals_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                SerilogRequestLogging.SerilogSetup(args);

                Log.Information("Starting host.");

                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly.");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();
    }
}
