﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReferralsController : ControllerBase
    {
        private readonly IReferralsService _referralsService;
        public ReferralsController(IReferralsService referralsService)
        {
            _referralsService = referralsService;
        }

        [HttpGet("user/{userId}")]
        public async Task<ActionResult<IEnumerable<ReferralResponseDTO>>> GetReferrals(long userId)
        {
            var referrals = await _referralsService.GetReferrals(userId);

            var response = new Response(referrals);
            return Ok(response);
        }

        [HttpGet("user/{userId}/credit")]
        public async Task<ActionResult<decimal>> GetCredit(long userId)
        {
            var credit = await _referralsService.GetCredit(userId);

            var response = new Response(credit);
            return Ok(response);
        }

        [HttpGet("user/{userId}/earningDetails")]
        public async Task<ActionResult<EarningDetailsResponseDTO>> GetEarningDetils(long userId)
        {
            var earningDetails = await _referralsService.GetEarningDetails(userId);

            var response = new Response(earningDetails);
            return Ok(response);
        }
    }
}