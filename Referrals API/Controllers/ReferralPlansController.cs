﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReferralPlansController : ControllerBase
    {
        private readonly IReferralPlansService _referralPlansService;

        public ReferralPlansController(IReferralPlansService referralPlansService)
        {
            _referralPlansService = referralPlansService;
        }

        [Authorize(Policy = "Admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReferralPlanResponseDTO>>> GetReferralPlans()
        {
            var referralPlans = await _referralPlansService.GetReferralPlans();

            var response = new Response(referralPlans);
            return Ok(response);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<IEnumerable<ReferralPlanResponseDTO>>> GetReferralPlans(long userId)
        {
            var referrals = await _referralPlansService.GetReferralPlans(userId);

            var response = new Response(referrals);
            return Ok(response);
        }

        [HttpPost]
        public async Task<ActionResult<string>> CreateNewReferralPlan([FromBody] NewReferralPlanRequestDTO newPlan)
        {
            var signupUrlWithReferal = await _referralPlansService.CreateNewReferralPlan(newPlan);

            var response = new Response(signupUrlWithReferal);
            return Ok(response);
        }
    }
}