﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Referrals.Common;
using System;

namespace Referrals.API.Controllers
{
    // Do not show this controller in swagger, it's only used internally for global exception handling
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorsController : ControllerBase
    {
        private readonly ILogger<ErrorsController> _logger;

        public ErrorsController(ILogger<ErrorsController> logger)
        {
            _logger = logger;
        }
        public ActionResult Index()
        {
            var statusCode = 500;

            var response = new Response();

            var exceptionHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            if (exceptionHandlerFeature?.Error != null)
            {
                var exception = exceptionHandlerFeature?.Error;
                response.Error = exception.Message;

                switch (exception)
                {
                    case ArgumentException _:
                        statusCode = 400;
                        break;
                    case UnauthorizedAccessException _:
                        statusCode = 403;
                        break;
                    default:
                        statusCode = 500;
                        break;
                }

                response.StatusCode = statusCode;

                _logger.LogError(exception, response.Error);
            }

            return StatusCode(statusCode, response);
        }
    }
}