﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;
        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [Authorize(Policy = "Admin")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserResponseDTO>>> Get()
        {
            var credit = await _usersService.Get();

            var response = new Response(credit);
            return Ok(response);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<UserDetailsResponseDTO>> Get(long userId)
        {
            var credit = await _usersService.Get(userId);

            var response = new Response(credit);
            return Ok(response);
        }
    }
}