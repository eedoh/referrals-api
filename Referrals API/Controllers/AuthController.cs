﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Services;
using Referrals_API.DTOs;
using System.Threading.Tasks;

namespace Referrals.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("signup")]
        public async Task<ActionResult<long>> SignUp([FromBody] SignupRequestDTO signupRequest)
        {
            var userId = await _authService.Signup(signupRequest);

            var response = new Response(userId);
            return Ok(response);
        }

        [Authorize(Policy = "Admin")]
        [HttpPost]
        public async Task<ActionResult<long>> Create([FromBody] CreateUserRequestDTO createUserRequest)
        {
            var userId = await _authService.CreateUserIdentity(createUserRequest);

            var response = new Response(userId);
            return Ok(response);
        }
    }
}