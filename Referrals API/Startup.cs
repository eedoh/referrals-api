using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Referrals.Database;
using Referrals_API.Configs;
using Referrals_API.Configurations;
using Referrals_API.Extensions;
using Serilog;
using System.Linq;

namespace Referrals_API
{
    public class Startup
    {
        private const string ALLOW_CORS_POLICY = "AllowCORSPolicy";
        private const string ALLOWED_DOMAINS = "AllowedDomains";
        private const string CONNECTION_STRING = "DefaultConnection";
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. It's purpose is adding services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // DbContext for PostgresSQL
            services.AddDbContext<IReferralsDBContext, ReferralsDBContext>(opts =>
            {
                opts.UseNpgsql(_configuration.GetConnectionString(CONNECTION_STRING));
            });

            // Swagger
            services.ConfigureSwagger();

            // Dependency Injection
            services.RegisterTypes(_configuration);

            // CORS
            services.AddCors(options =>
            {
                options.AddPolicy(ALLOW_CORS_POLICY,
                builder =>
                {
                    builder.WithOrigins(_configuration.GetValue<string>(ALLOWED_DOMAINS))
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            // Cognito Oauth2
            services.ConfigureAWSCognitoAuthentication(_configuration);

            // Controllers
            services.AddControllers().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. It's purpose is configuruing the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Configure Swagger UI and open API
            app.UseOpenApi(settings =>
            {
                settings.Path = "/swagger/" + "v1" + "/swagger.json";
                settings.PostProcess = (document, request) =>
                {
                    document.Host = request.Headers["X-Forwarded-Host"].FirstOrDefault();

                };
            });
            app.UseSwaggerUi3(options =>
            {
                options.DocumentPath = "/swagger/" + "v1" + "/swagger.json";
            });

            // Custom auth context, makes request headers available to the services
            app.UseMiddleware<AuthContextMiddleware>(); ;

            // global exception handler will be set up as ErrorsController
            app.UseExceptionHandler("/api/errors");

            app.UseHttpsRedirection();

            app.UseSerilogRequestLogging(opts =>
            {
                opts.EnrichDiagnosticContext = SerilogRequestLogging.EnrichFromRequest;
                opts.GetLevel = SerilogRequestLogging.GetLevel(Serilog.Events.LogEventLevel.Verbose, "Health checks");
            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseCors(ALLOW_CORS_POLICY);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
