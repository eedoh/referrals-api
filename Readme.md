﻿# Referrals API, Amitree take home task, by Irhad Babic

[![N|Solid](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR67VUfPgRoZ37cpERY4PZyzM-Y__IKn-Y1Tvcnyu4zsfC09g76&s)]

The API provides a back end solution for a skilled front end developer to build a referral program which whill allow users to use
their referral codes for earning referral fees when other sign up using those codes. 
	
	- New referral plan can be created for each individual user, specifying the referral fee they would earn, number of signed up users required for the specified referral fee ("PayoutUnitSize"), as well as the signup bonus amount new users will earn upon successful sign up (if they used the referral code)
	- New users can sign up without using any referral code, in which case they won't receive any bonuses. If they use a referral code, they will earn the amount specified in the referral plan as "SignupBonus"
	- After number of users, specified in the referral plan behind the referral code, signs up, the referrer will earn the specified referral fee. 
	- Their credit as well as earning details can be retreived through the API (/api/referrals)
	- A new user can only use one referral code when signing up
	- Users who sign up themselves will automatically be added to the "User" user group, and they will only be allowed to access their own data
	- Admin users can create other Admin users (assign them to "Admin" user group)
	- Admin users can retreive anyone's data


# How to interact with the running version of the API
These instructions assume Postman being used as a Rest API client, but any other client app that allows for OAuth2 authentication will do fine.

	- Make sure native version of Postman is installed on your system, and open it
	- Import Referrals API configuration and create a new Postman collection out of it
	    - Click "Import" in the upper left corner of the Postman UI
	    - In the import modal select "Import From Link" tab
	    - Provide link to the Swagger JSON document at "http://referralsapi-dev.us-west-2.elasticbeanstalk.com/swagger/v1/swagger.json"
	    - Check both options on the next modal
	    - In the left sidebar, click on the ellipsis next to the newly created collection, and select "Edit" from the context menu
	    - Select "Variables" tab, and change baseUrl to "http://referralsapi-dev.us-west-2.elasticbeanstalk.com" and save it
	- Make sure your Postman client properly authenticates each of the requests (except for Signup endpoint)
	    - Click on the ellipsis button once again in the left sidebar, and select "Edit" option from the context menu
	    - Select "Authorization" tab
	    - Select OAuth2.0 from the "TYPE" dropdodwn, and leave the other dropdown set to "Request Headers"
	    - Click on Get New Access Token and fill in the fields with following values, and click "Request Token"
	        - Token Name: Set to your liking
	        - Grant Type: Implicit
	        - Callback URL: https://amitree.com
	        - Auth URL: https://referrals-api.auth.us-west-2.amazoncognito.com/oauth2/authorize
	        - Client ID: 3t3m2h8skkik363s6en8sv5od6
	        - Scope: <empty>
	        - State: <empty>
	        - Client Authentication: Send as Basic Auth header
		- On the next screen use "admin@yopmail.com" and "Test123!" as credentials, if you want to authenticate with the seeded Admin account. 
		  You can use other credentials, if you already signed up a user on the system.
		- Select the last token on the left hand side of the newly opened modal, and click on "Use Token" at the bottom of it.
		- Click on "Update" on the underlying "Edit Collection" modal to return back to the main screen
		- Expand and visit each endpoint in the collection, select their "Authorization" tab, and select "Inherit auth from parent" in the TYPE dropdown
	- You are ready to test the API
	- Bear in mind
		- Not to forget replacing default placeholders for parameters with the actual values (Params tab for GET requests, Body tab for POST requests)
		- Access token is valid for an hour. After that you will start receiving 401(Unauthorized) status code as a response. When this happens, just 
		  renew your token for the collection following the steps above
		- All endpoints except for signup endpoint require authentication
		- Signup and Create User endpoints will return userId for newly created users as "data" property in the response. You should probably write these 
		  down, so you can use them later on, as parameters to other API calls


# How to run the code locally
1. Open the solution in Visual Studio (Community, Professional or Enterprise)
2. Set Referrals.API as startup project
3. Expand Referrals.API project and open appsettings.Development.json file, and change the DefaultConnection connection string to your local Postgres instance
4. Open the Package Manager Console (Tools -> Nuget Package Manager -> Package Manager Console)
5. Select Referrals.Database project in the dropdown shown on top of the Package Manager Console
6. Run "Update-Database" command from the Package Manager Console
7. Start debugging the project
8. When prompted about trusting self signed certificates issued by IIS Express, click "Yes"
9. SwaggerUI will show up as the default page
10 To test locally ran instance of the API, via Postman, you'll have to turn off "SSL certificate verification" in it's "General -> Settings" menu.
11. To run automated tests use the Test Explorer in Visual Studio or directly go to Test -> Run All Tests menu option, or open the Test explorer and run/debug tests per your preference 


# API Endpoints
For reviewer's convenience, swagger UI has been set up, so overview of all the endpoints available is effortless.
It can be accessed at http://referralsapi-dev.us-west-2.elasticbeanstalk.com/swagger
Regardless, here is a short description on each one of them

There are 4 controllers exposed

    - Users - used for easier overview of all the users on the system. Consists of two endpoints
	    - GET /api/Users - lists all the users on the system. Only Admins are allowed to to this.
	    - GET /api/Users/{userId} - provides more details about single user, whose id has been provided as a parameter
    - Auth - used for identity management, in this case, it only consists of the following endpoint
        - Signup - allows for signing up new users, with, or without referral codes. This is the only public endpoint, all other endpoints require the user to be authenticated.
	    - CreateUser - allows admin users creating new users, and assigning them to one of currently two existing user groups, User, and Admin
    - ReferralPlans - used for handling referral plans
        - GET /api/ReferralPlans - Lists all the referral plans on the system
        - POST /api/ReferralPlans - Creates a new referral plan for a user, specifying the amount referrer would earn, number of users per which they will earn their 
		  referral fees, as well as the signup bonus new user will receive if they use the code for signup.
            - This endpoint also allows for inserting "brand phrases" into the code. So, for example, if we'd like for our new referral code to include word "FOLIO",  we can specify that as a "brand phrase" parameter. 
            - Referral code will prepend the brand phrase to random array of characters required for referral code to have specified length. 
            - If the brand phrase's length is equal to the specified code length, the brand phrase will become the referral code.
        - GET /api/ReferralPlans/{userId} - Lists all the referral plans assigned to the user with the specified userId
    - Referrals - used for providing details on all referrals for any user on the system. It consists of three endpoints
		- GET /api/Referrals/user/{userId} - Lists all the referrals specified user acrued over time. Only Admins are allowed to do this.
        - GET /api/Referrals/user/{userId}/credit - Returns total amount of money user has earned over time.
            - This is being calculated as signup bonus (if the user signed up using someone else's referral code) plus the amount earned by referring other users.
        - GET /api/Referrals/user/{userId}/earningDetails - provides detailed breakdown on user's earnings (signup bonus in addition to credits earned per each of their referral codes, as well as all the referrals who used each of the codes.)