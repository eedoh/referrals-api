﻿using Referrals.Common.DTOs;
using Referrals_API.DTOs;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public interface IAuthService
    {
        Task<CreateUserResponseDTO> Signup(SignupRequestDTO request);
        Task<CreateUserResponseDTO> CreateUserIdentity(CreateUserRequestDTO createUserDTO);
    }
}
