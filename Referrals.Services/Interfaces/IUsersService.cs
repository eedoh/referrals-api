﻿using Referrals.Common.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Referrals.Services.Interfaces
{
    public interface IUsersService
    {
        Task<IEnumerable<UserResponseDTO>> Get();
        Task<UserDetailsResponseDTO> Get(long userId);
        Task<long> CreateUser(CreateUserRequestDTO request);
    }
}
