﻿namespace Referrals.Services
{
    public interface IHelperService
    {
        string GetNewReferralCode(short length = 10, string brandPhrase = "");
    }
}
