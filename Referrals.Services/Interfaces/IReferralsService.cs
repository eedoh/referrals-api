﻿using Referrals.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public interface IReferralsService
    {
        Task<IEnumerable<ReferralResponseDTO>> GetReferrals(long userId);
        Task<decimal> GetCredit(long userId);
        Task<EarningDetailsResponseDTO> GetEarningDetails(long userId);
        Task CreateReferral(long userId, string referralCode);
    }
}
