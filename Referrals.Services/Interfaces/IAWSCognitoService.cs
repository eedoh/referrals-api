﻿using Amazon.CognitoIdentityProvider.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.Services.Interfaces
{
    public interface IAWSCognitoService
    {
        Task<AdminCreateUserResponse> SignUpUserAsync(string userName, string password, List<AttributeType> userAttributes);
        Task<AdminDeleteUserResponse> DeleteUserAsync(string username);
        Task<AdminAddUserToGroupResponse> AddUserToGroup(string username, string groupName);
        Task<bool> UserExists(string username);
    }
}
