﻿using Referrals.Common;
using Referrals.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public interface IReferralPlansService
    {
        Task<IEnumerable<ReferralPlanResponseDTO>> GetReferralPlans();
        Task<IEnumerable<ReferralPlanResponseDTO>> GetReferralPlans(long userId);
        Task<CreateReferralPlanResponseDTO> CreateNewReferralPlan(NewReferralPlanRequestDTO newPlan);
    }
}
