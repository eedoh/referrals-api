﻿using Microsoft.Extensions.Logging;
using Referrals.Database;
using System;
using System.Linq;

namespace Referrals.Services
{
    public class HelperService : IHelperService
    {
        private readonly IReferralsDBContext _context;
        private readonly ILogger<HelperService> _logger;

        public HelperService(IReferralsDBContext context, ILogger<HelperService> logger)
        {
            _context = context;
            _logger = logger;
        }
        public string GetNewReferralCode(short length = 10, string brandPhrase = "")
        {
            Random random = new Random();

            // Create random codes from alphanumeric characters
            char[] keys = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890".ToCharArray();
            string code;

            // If brand phrase length is equal to required code length, the phrase will become the referral code
            // Check if the code already exists in the DB, and throw an exception if it does
            if (length == brandPhrase.Length)
            {
                if (CodeAlreadyExists(brandPhrase))
                {
                    var exception = new Exception($"Could not create a new and unique referral code with the phrase \"{brandPhrase}\" and referral code length of {length}");

                    _logger.LogError(exception.Message, exception);
                    throw exception;
                }
                else
                {
                    _logger.LogInformation("Brand phrase being used as a referral code.");
                    return brandPhrase;
                }
                
            }

            // If the phrase length is shorter than desired code length, add random characters to it
            // Do this until the generated code is unique
            do
            {
                code = brandPhrase + Enumerable
                    .Range(1, length - brandPhrase.Length)
                    .Select(k => keys[random.Next(0, keys.Length - 1)])
                    .Aggregate("", (e, c) => e + c);

            } while (CodeAlreadyExists(code));

            return code;
        }

        private bool CodeAlreadyExists(string code)
        {
            return _context.ReferralPlans.Any(rc => rc.ReferralCode == code);
        }
    }


}
