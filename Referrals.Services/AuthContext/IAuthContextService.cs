﻿using Referrals.Common.DTOs;
using System.Threading.Tasks;

namespace Referrals.Services.AuthContext
{
    public interface IAuthContextService
    {
        Task<AuthContextUserDTO> Get(string sub, string group);
    }
}
