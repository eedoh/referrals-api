﻿using Referrals.Common.DTOs;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Services.AuthContext
{
    public class AuthContext : IAuthContext
    {
        private readonly IAuthContextService _authContextService;
        private AuthContextUserDTO _user;

        public Dictionary<string, object> Data { get; set; }

        public AuthContext(IAuthContextService authContextService)
        {
            _authContextService = authContextService;
        }

        public async Task<AuthContextUserDTO> GetUser()
        {
            if (_user == null) await SetUser();

            return _user;
        }

        public string GetAttribute(string attributeName)
        {
            if (!Data.TryGetValue(attributeName, out object value))
            {
                var claim = GetClaim(attributeName);

                if (claim != null)
                {
                    Data.Add(attributeName, claim);
                    value = claim;
                }
            }

            return (string)value;
        }

        public async Task ValidateUserAccessRights(long userId)
        {
            var requestUser = await GetUser();
            if (requestUser.Id != userId && requestUser.Group != "Admin") throw new UnauthorizedAccessException("Only admin user can access other account's information");
        }

        private object GetClaim(string claim)
        {
            object tokenObject = null;
            if (Data.TryGetValue("authorization", out tokenObject) || Data.TryGetValue("Authorization", out tokenObject))
            {
                var tokenWithoutBearer = tokenObject.ToString().Substring("Bearer ".Length);

                //decrypt token
                var token = new JwtSecurityToken(jwtEncodedString: tokenWithoutBearer);

                //get payload
                var _claim = token.Claims.FirstOrDefault(x => x.Type == claim)?.Value;

                return _claim;
            }

            return null;
        }

        private async Task SetUser()
        {
            var sub = GetAttribute("sub");
            var group = GetAttribute("cognito:groups");

            _user = await _authContextService.Get(sub, group);
        }
    }
}
