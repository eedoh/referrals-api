﻿using Microsoft.EntityFrameworkCore;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Database.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Services.AuthContext
{
    public class AuthContextService : IAuthContextService
    {
        private readonly IReferralsDBContext _context;

        public AuthContextService(IReferralsDBContext context)
        {
            _context = context;
        }
        public async Task<AuthContextUserDTO> Get(string sub, string group)
        {
            if (!_context.Users.Any(u => u.CognitoId == sub)) throw new ArgumentException("User does not exist");

            var user = await _context.Users
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.CognitoId == sub);

            return Map(user, group);
        }

        private AuthContextUserDTO Map(User user, string group)
        {
            return new AuthContextUserDTO
            {
                Id = user.Id,
                CognitoId = user.CognitoId,
                Group = group
            };
        }
    }
}
