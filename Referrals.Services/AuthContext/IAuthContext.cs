﻿using Referrals.Common.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.Services.AuthContext
{
    public interface IAuthContext
    {
        Dictionary<string, object> Data { get; set; }
        Task<AuthContextUserDTO> GetUser();
        string GetAttribute(string attributeName);
        Task ValidateUserAccessRights(long userId);
    }
}
