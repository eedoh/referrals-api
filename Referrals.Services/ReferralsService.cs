﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Database.Entities;
using Referrals.Services.AuthContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public class ReferralsService : IReferralsService
    {
        private readonly IReferralsDBContext _context;
        private readonly ILogger<ReferralsService> _logger;
        private readonly IAuthContext _authContext;

        public ReferralsService(IReferralsDBContext context, ILogger<ReferralsService> logger, IAuthContext authContext)
        {
            _context = context;
            _logger = logger;
            _authContext = authContext;
        }

        public async Task<IEnumerable<ReferralResponseDTO>> GetReferrals(long userId)
        {
            if (!_context.Users.Any(u => u.Id == userId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(userId);

            var referrals = GetAllUserReferrals(userId)
                .Select(r => new ReferralResponseDTO()
                {
                    FirstName = r.Referree.FirstName,
                    LastName = r.Referree.LastName,
                    EnrolledOn = r.Referree.CreatedDate,
                    ReferralCode = r.ReferralPlan.ReferralCode
                });

            return referrals;
        }

        public async Task<decimal> GetCredit(long userId)
        {
            if (!_context.Users.Any(u => u.Id == userId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(userId);

            // count referrals for each referral code used
            var referralCounts = GetAllUserReferrals(userId)
                .GroupBy(r => r.ReferralPlanId)
                .Select(r => new ReferralsCountDTO()
                {
                    ReferralPlanId = r.Key,
                    ReferralCode = r.FirstOrDefault().ReferralPlan.ReferralCode,
                    Amount = r.FirstOrDefault().ReferralPlan.Amount,
                    PayoutUnitSize = r.FirstOrDefault().ReferralPlan.PayoutUnitSize,
                    Count = r.Count()
                });

            // calculate earnings by each of those referral codes
            decimal referralsCredit = 0;
            referralCounts.ToList().ForEach(rc =>
            {
                var referralUnits = (rc.Count / rc.PayoutUnitSize);
                referralsCredit += decimal.Multiply((decimal)referralUnits, rc.Amount);
            });

            // get signup bonus amount, if applicable
            var signupBonus = await GetSignupBonus(userId);

            // return sum of the referral credit and signup bonus
            return referralsCredit + signupBonus;
        }

        public async Task<EarningDetailsResponseDTO> GetEarningDetails(long userId)
        {
            if (!_context.Users.Any(u => u.Id == userId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(userId);

            var earningDetails = new EarningDetailsResponseDTO();
            earningDetails.Signup = new SignupBonusResponseDTO()
            {
                EnrolledOn = (await _context.Users.FirstOrDefaultAsync(u => u.Id == userId)).CreatedDate,
                SignupBonus = await GetSignupBonus(userId)
            };

            earningDetails.Referrals = GetAllUserReferrals(userId)
                .GroupBy(r => r.ReferralPlanId)
                .Select(r => new ReferralBonusResponseDTO()
                {
                    ReferralCode = r.FirstOrDefault().ReferralPlan.ReferralCode,
                    PayoutUnitSize = r.FirstOrDefault().ReferralPlan.PayoutUnitSize,
                    NumberOfReferrals = r.Count(),
                    ReferralBonus = (r.Count() / r.FirstOrDefault().ReferralPlan.PayoutUnitSize) * r.FirstOrDefault().ReferralPlan.Amount,
                    Referrals = r.Select(rr => new ReferralResponseDTO()
                    {
                        FirstName = rr.Referree.FirstName,
                        LastName = rr.Referree.LastName,
                        EnrolledOn = rr.Referree.CreatedDate,
                        ReferralCode = rr.ReferralPlan.ReferralCode
                    })
                });

            return earningDetails;
        }

        public async Task CreateReferral(long userId, string referralCode)
        {
            // Get the referral plan by the referral code provided
            var referralPlan = await _context.ReferralPlans.FirstOrDefaultAsync(rc => rc.ReferralCode == referralCode);

            if (referralPlan == null) throw new Exception("Invalid referral code.");

            // Save the information about the referral, for the newly inserted user and their referrer, if the new user used a referral code, and the code is valid
            Referral newReferral = new Referral()
            {
                ReferralPlanId = referralPlan.Id,
                ReferrerId = referralPlan.UserId,
                ReferreeId = userId
            };
            _context.Referrals.Add(newReferral);
            await _context.SaveChangesAsync();
        }

        private async Task<decimal> GetSignupBonus(long userId)
        {
            var signupReferral = await _context.Referrals.Include(r => r.ReferralPlan).FirstOrDefaultAsync(r => r.ReferreeId == userId);

            return signupReferral != null ? signupReferral.ReferralPlan.SignupBonus : 0;
        }

        private List<Referral> GetAllUserReferrals(long userId)
        {
            var allUserReferrals = _context.Referrals
                .Include(r => r.ReferralPlan)
                .Include(r => r.Referree)
                .Where(r => r.ReferrerId == userId).ToList();

            return allUserReferrals;
        }
    }
}
