﻿using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Microsoft.Extensions.Logging;
using Referrals.Common.DTOs;
using Referrals.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public class AWSCognitoService : IAWSCognitoService
    {
        private readonly AWSConfig _awsConfig;
        private readonly ILogger<AWSCognitoService> _logger;
        private static AmazonCognitoIdentityProviderClient _cognitoClient;

        public AWSCognitoService(AWSConfig awsConfig, ILogger<AWSCognitoService> logger)
        {
            _awsConfig = awsConfig;
            _logger = logger;

            _cognitoClient = new AmazonCognitoIdentityProviderClient(awsConfig.AWSAccessKeyId, awsConfig.AWSSecretKey, RegionEndpoint.GetBySystemName(awsConfig.Region));
        }

        public async Task<AdminCreateUserResponse> SignUpUserAsync(string userName, string password, List<AttributeType> userAttributes)
        {
            userName = userName.ToLower();
            emailToLowerCase(userAttributes);
            AdminCreateUserRequest request = new AdminCreateUserRequest
            {
                UserAttributes = userAttributes,
                Username = userName,
                UserPoolId = _awsConfig.UserPoolId,
                TemporaryPassword = password,
                MessageAction = "SUPPRESS",
            };

            AdminCreateUserResponse response;
            try
            {
                response = await _cognitoClient.AdminCreateUserAsync(request);
                AdminSetUserPasswordRequest adminSetUserPasswordRequest = new AdminSetUserPasswordRequest
                {
                    Password = password,
                    Permanent = true,
                    Username = userName,
                    UserPoolId = _awsConfig.UserPoolId
                };
                await _cognitoClient.AdminSetUserPasswordAsync(adminSetUserPasswordRequest);
            }
            catch (ResourceNotFoundException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }
            catch (InvalidParameterException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }
            catch (NotAuthorizedException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }
            catch (PreconditionNotMetException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }
            catch (UsernameExistsException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }
            catch (UserNotFoundException ex)
            {
                _logger.Log(LogLevel.Error, ex.Message, ex);
                throw ex;
            }

            return response;
        }

        public async Task<AdminDeleteUserResponse> DeleteUserAsync(string username)
        {
            AdminDeleteUserRequest adminDeleteUserRequest = new AdminDeleteUserRequest
            {
                Username = username,
                UserPoolId = _awsConfig.UserPoolId
            };

            var ressponse = await _cognitoClient.AdminDeleteUserAsync(adminDeleteUserRequest);
            return ressponse;
        }

        public async Task<AdminAddUserToGroupResponse> AddUserToGroup(string username, string groupName)
        {
            AdminAddUserToGroupRequest adminAddUserToGroupRequest = new AdminAddUserToGroupRequest
            {
                GroupName = groupName,
                Username = username,
                UserPoolId = _awsConfig.UserPoolId
            };
            var response = await _cognitoClient.AdminAddUserToGroupAsync(adminAddUserToGroupRequest);

            return response;
        }

        public async Task<bool> UserExists(string username)
        {
            try
            {
                AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest
                {
                    Username = username,
                    UserPoolId = _awsConfig.UserPoolId
                };
                await _cognitoClient.AdminGetUserAsync(adminGetUserRequest);

                return true;
            }
            catch (UserNotFoundException)
            {
                return false;
            }
        }

        private void emailToLowerCase(List<AttributeType> userAttributes)
        {
            var email = userAttributes.Find(e => e.Name.ToLower() == "email");
            email.Value = email.Value.ToLower();
        }
    }
}
