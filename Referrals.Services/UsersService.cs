﻿using Microsoft.EntityFrameworkCore;
using Referrals.Common.DTOs;
using Referrals.Database;
using Referrals.Database.Entities;
using Referrals.Services.AuthContext;
using Referrals.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public class UsersService : IUsersService
    {
        private readonly IReferralsDBContext _context;
        private readonly IAuthContext _authContext;
        private readonly IReferralsService _referralsService;

        public UsersService(IReferralsDBContext context, IAuthContext authContext, IReferralsService referralsService)
        {
            _context = context;
            _authContext = authContext;
            _referralsService = referralsService;
        }
        public async Task<IEnumerable<UserResponseDTO>> Get()
        {
            var users = await _context.Users
                .AsNoTracking()
                .Select(u => new UserResponseDTO
                {
                    Id = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Phone = u.Phone,
                    EnrolledOn = u.CreatedDate.Date
                })
                .ToListAsync();

            return users;
        }

        public async Task<UserDetailsResponseDTO> Get(long userId)
        {
            if (!_context.Users.Any(u => u.Id == userId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(userId);

            var referralActivity = await _referralsService.GetEarningDetails(userId);

            var users = await _context.Users
                .Include(u => u.ReferredBy)
                .AsNoTracking()
                .Where(u => u.Id == userId)
                .Select(u => new UserDetailsResponseDTO()
                {
                    Id = u.Id,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    Phone = u.Phone,
                    EnrolledOn = u.CreatedDate.Date,
                    ReferredBy = u.ReferredBy.Referrer.Email,
                    ReferralActivity = referralActivity
                }).FirstOrDefaultAsync();

            return users;
        }

        public async Task<long> CreateUser(CreateUserRequestDTO request)
        {
            // We could not check for uniqueness through fluent validation for DTO objects, without reading the DB
            // Hence this check right below
            if (await _context.Users.AnyAsync(u => u.Email == request.Email)) throw new Exception("Email address already in use");

            // Create a new User entity with the received information, and save it to the DB
            var newUser = new User()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Phone = request.Phone,
            };
            _context.Users.Add(newUser);
            await _context.SaveChangesAsync();
            
            return newUser.Id;
        }
    }
}
