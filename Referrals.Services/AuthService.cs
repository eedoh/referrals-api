﻿using Amazon.CognitoIdentityProvider.Model;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Referrals.Common.DTOs;
using Referrals.Common.Validators;
using Referrals.Database;
using Referrals.Database.Entities;
using Referrals.Services.Interfaces;
using Referrals_API.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public class AuthService : IAuthService
    {
        private readonly IReferralsDBContext _context;
        private readonly ILogger<AuthService> _logger;
        private readonly IAWSCognitoService _awsCognitoService;
        private readonly IReferralsService _referralsService;
        private readonly IUsersService _usersService;

        public AuthService(IReferralsDBContext context, ILogger<AuthService> logger, IAWSCognitoService awsCognitoService, IReferralsService referralsService, IUsersService usersService)
        {
            _context = context;
            _logger = logger;
            _awsCognitoService = awsCognitoService;
            _referralsService = referralsService;
            _usersService = usersService;
        }

        public async Task<CreateUserResponseDTO> Signup(SignupRequestDTO request)
        {
            SignupRequestValidator signupRequestValidator = new SignupRequestValidator();
            await signupRequestValidator.ValidateAndThrowAsync(request);

            return await CreateUser(CreateUserRequestDTO.GetCreateUserRequestDTO(request, "User"));
        }

        public async Task<CreateUserResponseDTO> CreateUserIdentity(CreateUserRequestDTO request)
        {
            CreateUserRequestValidator createUserRequestValidator = new CreateUserRequestValidator();
            await createUserRequestValidator.ValidateAndThrowAsync(request);

            return await CreateUser(request);
        }

        private async Task<CreateUserResponseDTO> CreateUser(CreateUserRequestDTO request)
        {
            using (var transaction = await((DbContext)_context).Database.BeginTransactionAsync())
            {
                long newUserId;
                try
                {
                    // Insert the new user in the DB;
                    newUserId = await _usersService.CreateUser(request);

                    // Handle the referral if it's been provided with the signup request data
                    if(!string.IsNullOrEmpty(request.ReferralCode))
                    {
                        await _referralsService.CreateReferral(newUserId, request.ReferralCode);
                    }

                    // Then create user account on AWS Cognito User Pool
                    var response = await _awsCognitoService.SignUpUserAsync(request.Email, request.Password, new List<AttributeType>() { new AttributeType { Name = "email", Value = request.Email } });

                    // Add it to the appropriate Cognito Group
                    var groupName = (string.IsNullOrEmpty(request.Group)) ? "User" : request.Group;
                    await _awsCognitoService.AddUserToGroup(request.Email, groupName);

                    // Save it's Cognito ID into the DB
                    _context.Users.FirstOrDefault(u => u.Email == request.Email).CognitoId = response.User.Attributes.Find(a => a.Name == "sub").Value;
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    // If anything goes wrong, rollback the DB entry
                    await transaction.RollbackAsync();

                    // and make sure user does not get created on Cognito user pool
                    if (await _awsCognitoService.UserExists(request.Email))
                    {
                        await _awsCognitoService.DeleteUserAsync(request.Email);
                    }
                    
                    _logger.LogError(ex, "Exception while trying to save/singup a new user.");

                    // Bubble the exception up, so it's handled by the global exception handler and proper error response is sent to the client app.
                    throw; 
                }
                await transaction.CommitAsync();

                return new CreateUserResponseDTO { UserId = newUserId };
            }
        }
    }
}
