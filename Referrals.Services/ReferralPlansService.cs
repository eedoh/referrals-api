﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Referrals.Common;
using Referrals.Common.DTOs;
using Referrals.Common.Validators;
using Referrals.Database;
using Referrals.Database.Entities;
using Referrals.Services.AuthContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Referrals.Services
{
    public class ReferralPlansService : IReferralPlansService
    {
        private readonly IReferralsDBContext _context;
        private readonly IHelperService _helperService;
        private readonly IAuthContext _authContext;
        private readonly ILogger<ReferralPlansService> _logger;

        public ReferralPlansService(IReferralsDBContext context, IHelperService helperService, IAuthContext authContext, ILogger<ReferralPlansService> logger)
        {
            _context = context;
            _helperService = helperService;
            _authContext = authContext;
            _logger = logger;
        }

        public async Task<IEnumerable<ReferralPlanResponseDTO>> GetReferralPlans()
        {
            var referralPlans = GetFilteredReferralPlans();

            return referralPlans;
        }

        public async Task<IEnumerable<ReferralPlanResponseDTO>> GetReferralPlans(long userId)
        {
            if (!_context.Users.Any(u => u.Id == userId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(userId);

            var referralPlan = GetFilteredReferralPlans(userId);

            return referralPlan;
        }

        public async Task<CreateReferralPlanResponseDTO> CreateNewReferralPlan(NewReferralPlanRequestDTO newPlan)
        {
            if (!_context.Users.Any(u => u.Id == newPlan.UserId)) throw new ArgumentException("User does not exist");

            await _authContext.ValidateUserAccessRights(newPlan.UserId);

            NewReferralPlanRequestValidator newReferralPlanRequestValidator = new NewReferralPlanRequestValidator();
            await newReferralPlanRequestValidator.ValidateAndThrowAsync(newPlan);

            var referralCode = _helperService.GetNewReferralCode(newPlan.CodeLength, newPlan.BrandPhrase);
            _context.ReferralPlans.Add(new ReferralPlan
            {
                UserId = newPlan.UserId,
                Amount = newPlan.Amount,
                PayoutUnitSize = newPlan.PayoutUnitSize,
                ReferralCode = referralCode,
                SignupBonus = newPlan.SignupBonus
            });

            long referralPlanId;
            try
            {
                referralPlanId = await _context.SaveChangesAsync();
            }
            // In real world use-case we'd probably have finer exception handling, and not simply catch ALL EXCEPTIONS
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception while trying to save a new referral plan.");

                // Bubble the exception up, so it's handled by the global exception handler (ErrorsController)
                // and proper error response is sent to the client app.
                throw;
            }

            return new CreateReferralPlanResponseDTO
            {
                ReferralPlanId = referralPlanId,
                ReferralCode = referralCode
            };
        }

        private IEnumerable<ReferralPlanResponseDTO> GetFilteredReferralPlans(long? userId = null)
        {
            var referralPlans = _context.ReferralPlans
                .Include(rc => rc.User)
                .AsNoTracking()
                .Where(rc => (userId != null)? rc.UserId == userId : rc.UserId != userId)
                .Select(rc => new ReferralPlanResponseDTO()
                {
                    Email = rc.User.Email,
                    Amount = rc.Amount,
                    PayoutUnitSize = rc.PayoutUnitSize,
                    SignupBonus = rc.SignupBonus,
                    ReferralCode = rc.ReferralCode
                }).ToList();

            return referralPlans;
        }
    }
}
