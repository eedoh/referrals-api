﻿using FluentValidation;
using Referrals_API.DTOs;

namespace Referrals.Common.Validators
{
    public class SignupRequestValidator : AbstractValidator<SignupRequestDTO>
    {
        public SignupRequestValidator()
        {
            RuleFor(x => x.Email).NotNull().NotEmpty().EmailAddress();
            RuleFor(x => x.FirstName).NotNull().NotEmpty();
            RuleFor(x => x.LastName).NotNull().NotEmpty();
            RuleFor(x => x.Password).NotNull().NotEmpty()
                .MinimumLength(8).MaximumLength(15)
                .Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z])")
                .WithMessage("Password must contain at least one uppercase letter, one lowercase letter, one number, and one special character.");
        }
    }
}
