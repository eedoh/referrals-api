﻿using FluentValidation;
using Referrals.Common.DTOs;

namespace Referrals.Common.Validators
{
    public class CreateUserRequestValidator : AbstractValidator<CreateUserRequestDTO>
    {
        public CreateUserRequestValidator()
        {
            RuleFor(x => x.Group)
                .Must((x,y) => x.Group == "Admin" || x.Group == "User")
                .Unless(x => x.Group == null || x.Group == "")
                .WithMessage("User group can only be 'User' or 'Admin'");
        }
    }
}
