﻿using FluentValidation;

namespace Referrals.Common.Validators
{
    public class NewReferralPlanRequestValidator : AbstractValidator<NewReferralPlanRequestDTO>
    {
        public NewReferralPlanRequestValidator()
        {
            RuleFor(x => x.PayoutUnitSize).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(x => x.Amount).NotNull().GreaterThan(0);
            RuleFor(x => x.SignupBonus).NotNull().NotEmpty().GreaterThan(0);
            RuleFor(x => x.CodeLength).NotNull().NotEmpty().GreaterThan((short)0);
            RuleFor(x => x.UserId).NotNull();
            RuleFor(x => x.BrandPhrase).Must((x, y) => x.BrandPhrase.Length > 0 && x.BrandPhrase.Length <= x.CodeLength)
                .When(x => !string.IsNullOrEmpty(x.BrandPhrase))
                .WithMessage("Brand phrase can't be longer than referral code length specified.");
        }
    }
}
