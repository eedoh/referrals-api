﻿namespace Referrals.Common.DTOs
{
    public class CreateUserResponseDTO
    {
        public long UserId { get; set; }
    }
}
