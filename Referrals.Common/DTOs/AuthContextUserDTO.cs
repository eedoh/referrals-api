﻿namespace Referrals.Common.DTOs
{
    public class AuthContextUserDTO
    {
        public long Id { get; set; }
        public string CognitoId { get; set; }
        public string Group { get; set; }
    }
}
