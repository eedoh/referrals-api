﻿namespace Referrals.Common.DTOs
{
    public class CreateReferralPlanResponseDTO
    {
        public long ReferralPlanId { get; set; }
        public string ReferralCode { get; set; }
    }
}
