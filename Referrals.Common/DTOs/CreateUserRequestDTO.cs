﻿using Referrals_API.DTOs;

namespace Referrals.Common.DTOs
{
    public class CreateUserRequestDTO : SignupRequestDTO
    {
        public string Group { get; set; }

        public static CreateUserRequestDTO GetCreateUserRequestDTO(SignupRequestDTO signupRequestDTO, string groupName)
        {
            return new CreateUserRequestDTO
            {
                FirstName = signupRequestDTO.FirstName,
                LastName = signupRequestDTO.LastName,
                Email = signupRequestDTO.Email,
                Password = signupRequestDTO.Password,
                Phone = signupRequestDTO.Phone,
                ReferralCode = signupRequestDTO.ReferralCode,
                Group = groupName
            };
        }
    }
}
