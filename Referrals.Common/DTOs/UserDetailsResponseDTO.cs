﻿namespace Referrals.Common.DTOs
{
    public class UserDetailsResponseDTO : UserResponseDTO
    {
        public string ReferredBy { get; set; }
        public EarningDetailsResponseDTO ReferralActivity { get; set; }
    }
}
