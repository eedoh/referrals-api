﻿namespace Referrals.Common.DTOs
{
    public class ReferralPlanDTO
    {
        public decimal Amount { get; set; }
        public int PayoutUnitSize { get; set; }
        public decimal SignupBonus { get; set; }
    }
}
