﻿namespace Referrals.Common.DTOs
{
    public class AWSConfig
    {
        public string Profile { get; set; }
        public string Region { get; set; }
        public string UserPoolId { get; set; }
        public string AppClientId { get; set; }
        public string APIClientId { get; set; }
        public string APIClientSecret { get; set; }
        public string AWSAccessKeyId { get; set; }
        public string AWSSecretKey { get; set; }
    }
}
