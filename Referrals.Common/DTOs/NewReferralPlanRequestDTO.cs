﻿using Referrals.Common.DTOs;

namespace Referrals.Common
{
    public class NewReferralPlanRequestDTO : ReferralPlanDTO
    {
        public long UserId { get; set; }
        public string BrandPhrase { get; set; }
        public short CodeLength { get; set; }
    }
}
