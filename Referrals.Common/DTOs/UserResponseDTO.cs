﻿using System;

namespace Referrals.Common.DTOs
{
    public class UserResponseDTO
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime EnrolledOn { get; set; }
    }
}
