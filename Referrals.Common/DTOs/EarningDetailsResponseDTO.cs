﻿using System.Collections.Generic;

namespace Referrals.Common.DTOs
{
    public class EarningDetailsResponseDTO
    {
        public SignupBonusResponseDTO Signup { get; set; }
        public IEnumerable<ReferralBonusResponseDTO> Referrals { get; set; }
    }
}
