﻿using System;

namespace Referrals.Common.DTOs
{
    public class SignupBonusResponseDTO
    {
        public DateTime EnrolledOn { get; set; }
        public decimal SignupBonus { get; set; }
    }
}
