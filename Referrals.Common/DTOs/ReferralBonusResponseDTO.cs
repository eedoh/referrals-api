﻿using System.Collections.Generic;

namespace Referrals.Common.DTOs
{
    public class ReferralBonusResponseDTO
    {
        public string ReferralCode { get; set; }
        public int PayoutUnitSize { get; set; }
        public int NumberOfReferrals { get; set; }
        public decimal ReferralBonus { get; set; }
        public IEnumerable<ReferralResponseDTO> Referrals { get; set; }
    }
}
