﻿namespace Referrals.Common.DTOs
{
    public class ReferralsCountDTO
    {
        public long ReferralPlanId { get; set; }
        public string ReferralCode { get; set; }
        public decimal Amount { get; set; }
        public int PayoutUnitSize { get; set; }
        public int Count { get; set; }
    }
}
