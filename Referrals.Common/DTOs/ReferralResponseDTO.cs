﻿using System;

namespace Referrals.Common.DTOs
{
    public class ReferralResponseDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime EnrolledOn { get; set; }
        public string ReferralCode { get; set; }
    }
}
