﻿namespace Referrals.Common.DTOs
{
    public class ReferralPlanResponseDTO : ReferralPlanDTO
    {
        public string Email { get; set; }
        public string ReferralCode { get; set; }
    }
}
