﻿namespace Referrals.Common
{
    public class Response
    {
        public virtual int StatusCode { get; set; }
        public virtual string Error { get; set; }
        public virtual object Data { get; set; }

        public Response(object data = null, string error = null, int statusCode = 200)
        {
            Data = data;
            Error = error;
            StatusCode = statusCode;
        }
    }
}
